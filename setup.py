# -*- coding: utf-8 -*-

from distutils.core import setup
from glob import glob
import subprocess

scripts = glob('scripts/*')
command = ['git', 'describe', '--tags']
version = subprocess.check_output(command).decode().strip()

setup(name='simple-longitudinal-analysis',
      version=version,
      description='Some simple longitudinal analysis functions',
      author='Shuo Han',
      author_email='shan50@jhu.edu',
      scripts=scripts,
      install_requires=['scipy', 'numpy', 'pandas', 'rpy2'],
      packages=['simple_longitudinal_analysis'])
