# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from scipy.stats import ttest_ind, fisher_exact
from collections import OrderedDict

P_THRES = 0.05


class MatcherFactory:

    @staticmethod
    def get_matcher(target_df, mode='age_sex'):
        """Select matcher
        
        Args:
            target_df (pandas.DataFrame): Match dataframe to this
            mode (str): The matching mode; in {'age_sex', 'age', 'sex'}

        Returns:
            matcher (.Matcher): Matcher instance

        Raises:
            RuntimeError: when mode is not in {'age_sex', 'age', 'sex'}

        """
        if mode == 'age_sex':
            return AgeSexMatcher(target_df)
        elif mode == 'age':
            return AgeMatcher(target_df)
        elif mode == 'sex':
            return SexMatcher(target_df)
        else:
            raise RuntimeError('`mode` should be in {"age_sex", "age", "sex"}')


class Matcher:
    """Match records
    
    Attributes:
        target_df (pandas.DataFrame): Select records to match this dataframe

    """
    def __init__(self, target_df):
        """Initialize

        """
        self.target_df = target_df
        # self.target_df.loc[target_df['sex']=='F', 'sex'] = 0
        # self.target_df.loc[target_df['sex']=='M', 'sex'] = 1
        self.num_records = len(target_df)

    def match_from(self, source_df):
        """Match `source_df` to `self.target_df`

        Args:
            source_df (pandas.DataFrame): Match from this

        Returns:
            selected (pandas.DataFrame): The matched dataframe

        """
        visited = self._create_visited(source_df)
        matched = pd.concat([self._match_from(source_df, visited)
                             for i in range(self.num_records)])
        return matched 

    def _create_visited(self, source_df):
        """Create a series to keep track of visited records

        Args:
            source_df (pandas.DataFrame): The df to match from

        Returns:
            visited (pandas.Series): A series with the same indicies with
                source_df used to indicate the visited records

        """
        return pd.Series(np.zeros(len(source_df)), index=source_df.index)

    def _match_from(self, source_df, visited):
        raise NotImplementedError

    def validate(self, matched):
        """Validate the matching

        Args:
            matched (pandas.DataFrame): The matched dataframe to validate

        """
        raise NotImplementedError


class AgeMatcher(Matcher):
    """Match age

    Attributes:
        target_df (pandas.DataFrame): Select records to match this dataframe

    """
    def __init__(self, target_df):
        """Initialize

        """
        super().__init__(target_df)
        self._init_age()

    def _init_age(self):
        self._age_mean = self.target_df['age'].mean()
        self._age_std = self.target_df['age'].std()

    def _match_from(self, source_df, visited):
        """Match `source_df` to `self.target_df`

        Args:
            source_df (pandas.DataFrame): Match from this

        Returns:
            matched (pandas.DataFrame): The matched dataframe

        """
        age = self._sample_age()
        ind = (source_df['age'] - age + visited).abs().idxmin()
        visited[ind] = float('inf')
        return source_df.loc[[ind]]

    def _sample_age(self):
        """Sample age from a normal distribution
        
        Returns:
            age (float): The sampled age

        """
        return np.random.normal(self._age_mean, self._age_std)

    def validate(self, matched):
        """Validate if matched age has the same distribution as the traget

        Use Welch’s t-test to validate if the two have the same normal
        distribution

        Args:
            matched (pandas.DataFrame): The matched dataframe to validate

        Returns:
            pvalue (float): The pvalue of the t-test
            is_matched (bool): True if they have no difference

        """
        result = OrderedDict()
        result['pvalue'], result['is_matched'] = self._validate_age(matched)
        return result

    def _validate_age(self, matched):
        result = ttest_ind(matched['age'], self.target_df['age'],
                           equal_var=False)
        is_matched = result.pvalue > P_THRES
        return result.pvalue, is_matched


class SexMatcher(Matcher):
    """Match sex

    Attributes:
        target_df (pandas.DataFrame): Select records to match this dataframe

    """
    def __init__(self, target_df):
        """Initialize

        """
        super().__init__(target_df)
        self._init_sex()

    def _init_sex(self):
        num_females = float((self.target_df['sex']==0).sum())
        self._sex_ratio = num_females / self.num_records

    def _match_from(self, source_df, visited):
        sex = self._sample_sex()
        ind = (source_df['sex'] - sex + visited).abs().idxmin()
        visited[ind] = float('inf')
        return source_df.loc[[ind]]

    def _sample_sex(self):
        """Sample sex from a binary distribution

        Return:
            sex (int): 0: female; 1: male
        """
        return 0 if np.random.uniform() < self._sex_ratio else 1

    def validate(self, matched):
        """Validate if the input has the same sex ratio with the target

        User Fisher's exact test to test if the num_females / num_subjects is
        the same for the input and the target dataframe

        Returns:
            pvalue (float): The pvalue of the test
            is_matched (bool): True if they have no difference

        """
        result = OrderedDict()
        result['pvalue'], result['is_matched'] = self._validate_sex(matched)
        return result

    def _validate_sex(self, matched):
        contingency = np.array([matched['sex'].value_counts()[[1, 0]],
                                self.target_df['sex'].value_counts()[[1, 0]]])
        result = fisher_exact(contingency)
        pvalue = result[1]
        is_matched = pvalue > P_THRES
        return pvalue, is_matched


class AgeSexMatcher(AgeMatcher, SexMatcher):
    """Match age and sex

    Attributes:
        target_df (pandas.DataFrame): Select records to match this dataframe

    """
    def __init__(self, target_df):
        """Initialize

        """
        super().__init__(target_df)
        self._init_sex()

    def _match_from(self, source_df, visited):
        sex = self._sample_sex()
        sex_array = self._create_visited(source_df)
        sex_array[source_df['sex'] != sex] = float('inf')
        age = self._sample_age()
        ind = (source_df['age'] - age + visited + sex_array).abs().idxmin()
        visited[ind] = float('inf')
        return source_df.loc[[ind]]

    def validate(self, matched):
        """Validate matching using t-test and fisher's exact test

        Returns:
            result (OrderedDict): (key, value):
                age_pvalue (float): Pvalue for matching age
                age_matched (bool): True if age_pvalue > threshould
                sex_pvalue (float): Pvalue for matching sex
                sex_matched (bool): True if sex_pvalue > threshould

        """
        result = OrderedDict()
        result['age_pvalue'],result['age_matched'] = self._validate_age(matched)
        result['sex_pvalue'],result['sex_matched'] = self._validate_sex(matched)
        return result
