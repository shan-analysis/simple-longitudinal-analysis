# -*- coding: utf-8 -*-

import numpy as np


def select_age(df, lower_age, upper_age):
    df = df[(df['age'] >= lower_age) & (df['age'] <= upper_age)]
    return df


def select_random_subjects(df, number):
    subjects = df['participant_id'].unique()
    if len(subjects) > number:
        subjects = np.random.choice(subjects, size=number, replace=False)
        df = df[df['participant_id'].isin(subjects)]
    return df
