# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from copy import deepcopy
from pandas.core.reshape.util import cartesian_product


class DummyData:

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def get_data(self):
        combo = self._get_kwargs_combo()
        participants = self._create_participants(len(combo))
        combo.insert(0, 'participant_id', participants)
        return combo

    def _create_participants(self, total_num):
        participants = [('sub-%%0%dd' % len(str(total_num))) % i
                        for i in range(total_num)]
        return participants

    def _get_kwargs_combo(self):
        values = cartesian_product(self.kwargs.values())
        combo = pd.DataFrame({k: v for k, v in zip(self.kwargs.keys(), values)})
        return combo 


class AgeDummyData(DummyData):

    def __init__(self, ages, **kwargs):
        self.ages = pd.DataFrame(dict(age=ages))
        self.kwargs = kwargs

    def get_data(self):
        ages = self.ages.set_index([[0] * len(self.ages)])
        combo = super().get_data()
        combo = combo.set_index([[0] * len(combo)])
        result = combo.join(ages, how='outer')
        return result


class BaselineAgeDummyData(AgeDummyData):

    def __init__(self, baseline_ages, followup_times, **kwargs):
        self.ages = pd.DataFrame(dict(followup_time=followup_times))
        self.kwargs = kwargs
        self.kwargs['baseline_age'] = baseline_ages
