# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from collections import defaultdict, OrderedDict


class Characters:
    """Subject characters to generate the toy data

    Attributes:
        _characters (defaultdict(dict)): Hold the characters

    """
    def __init__(self):
        self._characters = defaultdict(dict)

    def add_character(self, name, type, **kwargs):
        """Add character

        Args:
            name (str): The name of the character (age, etc.)
            type (str): Distribution type {"bernoulli", "normal"}
            kwargs (dict): The parameters of the distribution; "mean" and "std"
                for normal distribution, "p" for bernoulli

        """
        if type == 'bernoulli':
            if 'p' not in kwargs:
                raise RuntimeError('bernoulli distribution should input p')
        elif type == 'normal':
            if ('mean' not in kwargs) or ('std' not in kwargs):
                raise RuntimeError('normal distribution should input mean, std')
        else:
            raise RuntimeError('type should be in {"bernoulli", "normal"}')

        self._characters[name]['type'] = type
        self._characters[name].update(kwargs)

    def items(self):
        return self._characters.items()

    def __contains__(self, item):
        return self._characters.__contains__(item)

    def pop(self, key):
        return self._characters.pop(key)


class FixedEffects:
    """Fixed effects for creating ToyData

    Attributes:
        _effects (list): Hold the effects

    """
    def __init__(self):
        self._effects = list()

    def add_effect(self, name, value=0, std_error=1, center=0):
        effect = {'name': name, 'Value':value, 'Std.Error':std_error,
                  'Center':center}
        self._effects.append(effect)

    def get_effects(self):
        df = pd.DataFrame(self._effects)
        df = df.set_index('name')
        return df


class RandomEffects:
    """Random effects for the lme model

    Random effect is characterized by a covariance matrix. Input the std of
    the variable 1 and variable 2, and the correlation between 1 and 2

    Args:
        _covariance (pd.DataFrame): The covariance matrix; column and index name
            are the covariates

    """
    def __init__(self):
        self._covariance = pd.DataFrame()
    
    def add_std(self, name, std):
        """Add effect std

        Args:
            name (str): The name of the covariate
            std (float): The std of the covariate

        """
        self._covariance.loc[name, name] = std ** 2

    def add_correlation(self, name1, name2, corr):
        """Add the correlation between name1 and name2

        Args:
            name1, name2 (str): The name of the covariates
            corr (float): The correlation between name1 and name2

        """
        std1 = np.sqrt(self._covariance.loc[name1, name1])
        std2 = np.sqrt(self._covariance.loc[name2, name2])
        self._covariance.loc[name1, name2] = corr * std1 * std2
        self._covariance.loc[name2, name1] = corr * std1 * std2

    def get_effects(self):
        return self._covariance.fillna(0)


class ToyData:
    """Random toy data to validate lme/lme and spaghetti plot

    Assume lm model if self.random is None

    Attributes:
        characters (.Characters): Subject characters (age, sex, icv, etc.)
        fixed (.FixedEffects): Fixed effects for the lme and lm models
        random (.RandomEffects): Random effects for the lme model

    """
    def __init__(self, characters, fixed, error_std, random=None):
        """Initialize
        
        """
        self.characters = characters
        if 'followup_intervals' in self.characters:
            self.followup_intervals = self.characters.pop('followup_intervals')
        self.fixed = fixed
        self.random = random
        self.error_std = error_std

    def get_data(self, num_subjects, max_num_sessions=2):
        """Generate random data

        Args:
            num_subjects (int): The total number of subjects to generate
            max_num_sessions (int): The number of visits per subject will be
                randomly drawn from [1, `max_num_sessions`]

        """
        if max_num_sessions > 1 and not hasattr(self, 'followup_intervals'):
            message = 'Specify "followup_intervals" in self.characters '\
                    + 'with `max_num_sessions` > 1'
            raise RuntimeError(message)
        data = dict()
        data['participant_id'] = self._get_subject_ids(num_subjects)
        for name, character in self.characters.items():
            data[name] = self._draw(character, num_subjects)
        dataframe = pd.DataFrame(data)
        if max_num_sessions == 1:
            dataframe['session_id'] = 'ses-0'
        else:
            sessions = list()
            for i, row in dataframe.iterrows():
                tmp = OrderedDict()
                num_sessions = self._draw_num_sessions(max_num_sessions)
                fi = np.abs(self._draw(self.followup_intervals, num_sessions - 1))
                fi = np.insert(fi, 0, 0.0)
                tmp['followup_time'] = fi.cumsum()
                tmp['session_id'] = self._get_session_ids(num_sessions)
                tmp = pd.DataFrame(tmp)
                tmp.insert(0, 'participant_id', row['participant_id'])
                sessions.append(tmp)
            sessions = pd.concat(sessions)
            dataframe = dataframe.merge(sessions, on='participant_id')

        if not 'age' in dataframe:
            dataframe['age'] = dataframe['baseline_age'] + dataframe['followup_time']

        for effect in self.fixed.get_effects().index:
            if ':' in effect:
                columes = [c.strip() for c in effect.split(':')]
                subdf = dataframe[columes].copy()
                for c in columes:
                    subdf[c] -= self.fixed.get_effects().loc[c, 'Center']
                dataframe[effect] = subdf.prod(1)

        dataframe = dataframe.set_index(['participant_id', 'session_id'])
        dataframe['(Intercept)'] = 1
        tmp = dataframe[self.fixed.get_effects().index]
        tmp = tmp.sub(self.fixed.get_effects()['Center'], axis=1)
        sums = list()
        for name, group in tmp.groupby('participant_id'):
            coefs = dict()
            for effect in tmp.columns:
                mean = self.fixed.get_effects().loc[effect, 'Value']
                std = self.fixed.get_effects().loc[effect, 'Std.Error']
                coef = self._draw_from_normal(dict(mean=mean, std=std), 1)[0]
                coefs[effect] = coef
            coefs = pd.Series(coefs)
            sum = group.dot(coefs)
            sums.append(sum)
        sums = pd.concat(sums)
        dataframe['value'] = sums
        errors = self._draw_from_normal(dict(mean=0, std=self.error_std),
                                        len(dataframe))
        dataframe['value'] = dataframe['value'] + errors
        return dataframe

    def _draw(self, character, num_samples):
        """Draw from a ditribution

        Args:
            character (dict): Character with key "type" (see class Characters)
            num_samples (int): The number of samples drawn from the distribution

        Returns:
            result (numpy.array): The drawn samples

        """
        if character['type'] is 'bernoulli':
            return self._draw_from_bernoulli(character, num_samples)
        elif character['type'] is 'normal':
            return self._draw_from_normal(character, num_samples)

    def _draw_from_bernoulli(self, character, num_samples):
        """Draw from bernoulli distribution

        Args:
            character (dict): Dict with keys "type", "p". "p" is the
                probability of 1 which is male if the character is sex
            num_samples (int): The num of samples drawn from the distribution

        Returns:
            result (numpy.array): The drawn samples

        """
        return np.random.binomial(1, character['p'], size=num_samples)

    def _draw_from_normal(self, character, num_samples):
        """Draw from a normal distribution

        Args:
            character (dict): Keys "type", "mean", and "std"
            num_samples (int): The number of samples drawn from the distribution

        Returns:
            result (num.arary): The drawn samples

        """
        return np.random.normal(loc=character['mean'], scale=character['std'],
                                size=num_samples)

    def _draw_num_sessions(self, max_num_sessions):
        candidates = np.arange(1, max_num_sessions+1)
        result = np.random.choice(candidates)
        return result

    def _get_subject_ids(self, num_subjects):
        """Get the ID of the generated subjects

        Args:
            num_subjects (int): The number of subjects to generate

        Return:
            ids (list): The generate ids

        """
        num_digits = len(str(num_subjects))
        return [('sub-%%0%dd' % num_digits) % i for i in range(num_subjects)]

    def _get_session_ids(self, num_sessions):
        """Get the ID of the generated sessions

        Args:
            num_sessions (int): The number of sessions to generate

        Return:
            ids (list): The generate ids

        """
        num_digits = len(str(num_sessions))
        return [('ses-%%0%dd' % num_digits) % i for i in range(num_sessions)]
