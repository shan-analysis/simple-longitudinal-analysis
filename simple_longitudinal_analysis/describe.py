#!/usr/bin/env python
# -*- coding: utf-8 -*-

SEX = {'1': 'Male', '0': 'Female', 'F': 'Female', 'M': 'Male'}

def describe(df):
    if 'participant_id' in df:
        print('Number of selected subjects:',
              len(df.groupby('participant_id')))
        if 'session_id' in df:
            print('Number of selected records:',
                  len(df.groupby(['participant_id', 'session_id'])))
    if 'age' in df:
        print('Minimum age: %.2f;' % (df['age'].min(), ),
              'Maximum age: %.2f'  % (df['age'].max(), ))
    if 'participant_id' in df:
        baseline_ages = df.groupby('participant_id')['age'].min()
        print('Baseline age %.2f ± %.2f' % (baseline_ages.mean(),
                                             baseline_ages.std()))
    if 'sex' in df and 'participant_id' in df:
        for sex, group in df.groupby('sex'):
            print(SEX[str(sex)], len(group.groupby('participant_id')))
