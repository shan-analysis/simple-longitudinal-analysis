# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from .dummy_data import AgeDummyData as AD
from .dummy_data import BaselineAgeDummyData as BD

MALE_COLOR = 'b'
FEMALE_COLOR = 'r'
DEFAULT_COLOR = 'k'
colorful_set = {'male', 'female', 'all'}

#     @property
#     def _male(self):
#         if self.balance_sex:
#             return 0.5
#         else:
#             return 1
# 
#     @property
#     def _female(self):
#         if self.balance_sex:
#             return -0.5
#         else:
#             return 
# 
#     def __init__(self, balance_sex=False):
#         self.balance_sex = balance_sex
# 
class Plotter:

    def _plot_sex_legend(self, ax):
        print(self.markersize)
        lines = [Line2D([0], [0], color=FEMALE_COLOR, lw=1),
                 Line2D([0], [0], color=MALE_COLOR, lw=1)]
        legend = ax.legend(lines, ['Female', 'Male'])
        # legend.get_frame().set_edgecolor('gray')

    def _get_subject_sex(self, name, data):
        sex = data['sex'].unique()
        if len(sex) != 1:
            print('Subject', name, 'has sex', sex)
            sex = None
        else:
            sex = sex[0]
        return sex


class Spaghetti(Plotter):
    """Plot Spaghetti plots

    Attributes:
        dataframe (pandas.DataFrmae): The data to plot
        separate_sex (bool): Plot male and female separately
        markersize (int): Dot size in the plot
        _grouped_dataframe (pandas.GroupBy)

    """
    def __init__(self, dataframe, separate_sex='all', markersize=2,
                 highlight_markersize=10, linewidth=1, highlight_linewidth=5,
                 linestyle='-', alpha=1, show_subject=False, fontsize=5):
        """Initialize

        """
        self.dataframe = dataframe 
        self.separate_sex = separate_sex
        self.markersize = markersize
        self.linewidth = linewidth
        self.highlight_markersize = highlight_markersize
        self.highlight_linewidth = highlight_linewidth
        self.linestyle = linestyle
        self.fontsize = fontsize
        self.alpha = alpha
        self.show_subject = show_subject
        self._grouped_dataframe = self.dataframe.groupby('participant_id')

    def plot(self, x_var, y_var, highlight=list(), ax=None, y_unit='',
             prediction=None, highlight_only=False, legend=False):
        if ax is None:
            ax = plt.subplot(111)
        plt.sca(ax)
        for subject, data in self._grouped_dataframe:
            if str(subject) in highlight:
                if highlight_only:
                    markersize = self.markersize
                    linewidth = self.linewidth
                else:
                    markersize = self.highlight_markersize
                    linewidth = self.highlight_linewidth
            else:
                markersize = self.markersize
                linewidth = self.linewidth
                if highlight_only:
                    continue

            sex = self._get_subject_sex(subject, data)
            if sex is None:
                print('Skip...')
                continue 
            elif (sex in {1, 0.5, 'M'}) and self.separate_sex == 'female':
                continue
            elif (sex in {0, -0.5, 'F'}) and self.separate_sex == 'male':
                continue
            else:
                color = self._get_color(sex)
                if color is None:
                    print('Skip...')
                    continue
            plt.plot(data[x_var], data[y_var], 'o%s%s'%(self.linestyle, color),
                     markersize=markersize, alpha=self.alpha,
                     linewidth=linewidth)
            if prediction is not None:
                col_name = 'predict.participant_id'
                pred = prediction[prediction['participant_id']==subject]
                plt.plot(data[x_var], pred[col_name], color, alpha=0.5)
            if self.show_subject:
                plt.text(data[x_var].iloc[0], data[y_var].iloc[0], subject,
                         fontsize=self.fontsize)
            ax.set_xlabel(x_var.capitalize() + ' (years)')
            if y_unit:
                ylabel = '%s (%s)' % (y_var, y_unit)
            else:
                ylabel = y_var
            # ax.set_ylabel(ylabel.replace('_', ' '))
        if legend and self.separate_sex in colorful_set:
            self._plot_sex_legend(ax)
        return ax

    def _get_color(self, sex):
        if sex in {1, 0.5, 'M'}:
            if self.separate_sex in colorful_set:
                color = MALE_COLOR
            else:
                color = DEFAULT_COLOR
        elif sex in {0, -0.5, 'F'}:
            if self.separate_sex in colorful_set:
                color = FEMALE_COLOR
            else:
                color = DEFAULT_COLOR
        else:
            print('Subject', name, 'has invalid sex', sex)
            color = None
        return color


class LME(Plotter):

    def __init__(self, coef, dummy_data):
        self.coef = coef
        self.dummy_data = dummy_data
        self.dummy_data['(Intercept)'] = 1
        self._append_interaction_to_dummy()
        self._grouped_dummy = self.dummy_data.groupby('participant_id')
        print(list(self._grouped_dummy)[0][-1])
        print(list(self._grouped_dummy)[-1][-1])
        self.markersize = 1

    @classmethod
    def from_baseline_age(cls, coef, baseline_ages=range(50, 95, 5),
                          followup_times=range(0, 5), icv=0, balance_sex=False):
        sex = [-0.5, 0.5] if balance_sex else [0, 1]
        bd = BD(baseline_ages, followup_times, sex=sex, icv=[icv])
        return cls(coef, bd.get_data())

    @classmethod
    def from_age(cls, coef, ages=range(50, 100, 10), icv=0, balance_sex=False):
        sex = [-0.5, 0.5] if balance_sex else [0, 1]
        ad = AD(ages, sex=sex, icv=[icv])
        return cls(coef, ad.get_data())

    def _append_interaction_to_dummy(self):
        for var in self.coef.index:
            if ':'in var:
                columns = [self.dummy_data[v] for v in var.split(':')]
                self.dummy_data[var] = np.multiply.reduce(columns)

    def plot(self, ax=None, age_mean=65, linewidth=2, ylabel='', legend=False):
        if ax is None:
            ax = plt.subplot(111)
        for name, group in self._grouped_dummy:
            group = group[self.coef['Value'].index]
            sex = self._get_subject_sex(name, group)
            if sex in {1, 0.5, 'M'}:
                color = MALE_COLOR
            elif sex in {0, -0.5, 'F'}:
                color = FEMALE_COLOR
            values = group.dot(self.coef['Value'])
            print('Plotter: min', np.min(values), 'max', np.max(values))
            if 'baseline_age' in group and 'followup_time' in group:
                ages = group['baseline_age'] + group['followup_time']
            else:
                ages = group['age']
            ax.plot(ages+age_mean, values, color, linewidth=linewidth)
        ax.set_xlabel('Age (years)') 
        ax.set_ylabel(ylabel) 
        if legend:
            self._plot_sex_legend(ax)
        return ax
