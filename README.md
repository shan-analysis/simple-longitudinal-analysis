# Some simple longitudinal anaylsis functions

Some functions are implemented in Python with rpy2

1. `calc_icc_lme.py`: Calculate intra-class correlations (ICC) using linear mixed effect model. This can deal with unequal number of repeats and account for age/time effect.
2. `plot_spaghettie.py`: Plot the spaghetti plot. This script can plot males and females separately and select a subset of data to plot.
3. `plot_lme.py`: Plot the fitted linear mixed effect model against the age.
4. `summary_demographic.py`: Print demographic statistices of a dataframe.
5. `match_age_sex.py`: Match the age and sex of the source dataframe to a target dataframe.
6. `fit_lme.py`: Fit a linear mixed effect model using `rpy2`
7. `fit_lm.py`: Fit a linear regression using `rpy2`
