#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt

from simple_longitudinal_analysis.plotters import LME


coef = pd.DataFrame([{'Value': 115705}, {'Value': 0.05}, {'Value': -1638},
                     {'Value': -329}, {'Value': -246}, {'Value': -26},
                     {'Value': -6}],
                    index=['(Intercept)', 'icv', 'sex', 'baseline_age',
                           'followup_time', 'sex:followup_time',
                           'baseline_age:followup_time'])
print(coef)
lme = LME.from_baseline_age(coef)
lme.plot()
plt.show()
