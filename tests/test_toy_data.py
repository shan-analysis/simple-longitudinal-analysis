#!/usr/bin/env python
# -*- coding: utf-8 -*-


from simple_longitudinal_analysis.toy_data import FixedEffects, RandomEffects
from simple_longitudinal_analysis.toy_data import ToyData, Characters


# re = RandomEffects()
# re.add_std('var1', 10)
# re.add_std('var2', 20)
# re.add_correlation('var2', 'var2', -0.5)
# print(re.get_effects())

# fe = FixedEffects()
# fe.add_effect('(Intercept)', value=10, std_error=1)
# fe.add_effect('age', value=10, std_error=1)
# fe.add_effect('sex', value=10, std_error=1)
# fe.add_effect('icv', value=10, std_error=1)
# print(fe.get_effects())
# # cross-sectional
# 
# num_subjects = 3
# max_num_sessions = 1
# characters = Characters()
# characters.add_character('sex', 'bernoulli', p=0.5)
# characters.add_character('age', 'normal', mean=65, std=10)
# characters.add_character('icv', 'normal', mean=1400000, std=150000)
# 
# toy_data = ToyData(characters, fixed=fe)
# df = toy_data.get_data(num_subjects, max_num_sessions)
# print(df)
# # df.to_csv('toy.csv', index=False)

# longitudinal
fe = FixedEffects()
# fe.add_effect('(Intercept)', value=130000, std_error=500)
# fe.add_effect('sex', value=1200, std_error=800)
# fe.add_effect('icv', value=0.05, std_error=0.003, center=1400000)
# fe.add_effect('baseline_age', value=-300, std_error=30, center=65)
# fe.add_effect('followup_time', value=-300, std_error=30)
# fe.add_effect('baseline_age:followup_time', value=-7.5, std_error=2.5)
# fe.add_effect('sex:followup_time', value=-40, std_error=4)
# error_std = 1700

fe.add_effect('(Intercept)', value=130000, std_error=0)
fe.add_effect('sex', value=1200, std_error=0)
fe.add_effect('icv', value=0.05, std_error=0, center=1400000)
fe.add_effect('baseline_age', value=-300, std_error=0, center=65)
fe.add_effect('followup_time', value=-300, std_error=0)
fe.add_effect('baseline_age:followup_time', value=-7.5, std_error=0)
fe.add_effect('sex:followup_time', value=-300, std_error=0)
error_std = 1700

num_subjects = 1000
max_num_sessions = 10
characters = Characters()
characters.add_character('sex', 'bernoulli', p=0.5)
characters.add_character('baseline_age', 'normal', mean=65, std=15)
characters.add_character('followup_intervals', 'normal', mean=1.5, std=0.8)
characters.add_character('icv', 'normal', mean=1400000, std=150000)

toy_data = ToyData(characters, fe, error_std)
df = toy_data.get_data(num_subjects, max_num_sessions)
df.to_csv('toy.csv')
