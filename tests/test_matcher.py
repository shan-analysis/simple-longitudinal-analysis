#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

from simple_longitudinal_analysis import describe
from simple_longitudinal_analysis.matchers import MatcherFactory


df = pd.read_csv('../scripts/volumes.csv')
df = df.groupby('participant_id').first()
target_df = df[df['diagnosis']==1].reset_index()
source_df = df[df['diagnosis']==0].reset_index()
print()
print('Target')
describe(target_df)
print('M/F:', target_df['sex'].value_counts()[[1, 0]].values)
print('Age mean', target_df['age'].mean(), 'std', target_df['age'].std())
print('Source')
describe(source_df)
sex_counts = source_df['sex'].value_counts()
print('M/F:', source_df['sex'].value_counts()[[1, 0]].values)
print('Age mean', source_df['age'].mean(), 'std', source_df['age'].std())

age_matcher = MatcherFactory.get_matcher(target_df, mode='age')
sex_matcher = MatcherFactory.get_matcher(target_df, mode='sex')
age_sex_matcher = MatcherFactory.get_matcher(target_df, mode='age_sex')

print('-' * 80)
print('Match sex only')
sex_matched = sex_matcher.match_from(source_df)
describe(sex_matched)
print('M/F:', sex_matched['sex'].value_counts()[[1, 0]].values)
print('Age mean', sex_matched['age'].mean(), 'std', sex_matched['age'].std())
print('Sex match:', sex_matcher.validate(sex_matched))
print('Age match:', age_matcher.validate(sex_matched))

print('-' * 80)
print('Match age only')
age_matched = age_matcher.match_from(source_df)
describe(age_matched)
print('M/F:', age_matched['sex'].value_counts()[[1, 0]].values)
print('Age mean', age_matched['age'].mean(), 'std', age_matched['age'].std())
print('Sex match:', sex_matcher.validate(age_matched))
print('Age match:', age_matcher.validate(age_matched))

print('-' * 80)
print('Match age and sex together')
age_sex_matched = age_sex_matcher.match_from(source_df)
describe(age_sex_matched)
print('M/F:', age_sex_matched['sex'].value_counts()[[1, 0]].values)
print('Age mean', age_sex_matched['age'].mean(), 
      'std', age_sex_matched['age'].std())
print('Match:', age_sex_matcher.validate(age_sex_matched))
