#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
from simple_longitudinal_analysis import AgeDummyData, BaselineAgeDummyData

data = AgeDummyData(50, 90, 5, icv=[0], sex=[0, 1])
result = pd.DataFrame({'age': [1, 2, 3, 4, 5]})
print(data.get_data())

data = BaselineAgeDummyData(range(50, 70, 5), range(0, 5), icv=[0], sex=[0, 1])
result = pd.DataFrame({'age': [1, 2, 3, 4, 5]})
print(data.get_data())
