#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser(description='Summary subject demographics')
parser.add_argument('-i', '--input', required=True, help='Input table')
parser.add_argument('-s', '--delimiter', help='File delimiter', default=',')
parser.add_argument('-x', '--separate-sex', default=False, action='store_true',
                    help='Separate sex when displaying the results')
parser.add_argument('-d', '--separate-diagnosis',
                    default=False, action='store_true',
                    help='Separate diagnosis when displaying the results')
parser.add_argument('-p', '--print-multiple-diagnosis',
                    default=False, action='store_true',
                    help='Print multiple diagnosis')
parser.add_argument('-sub', '--subject-header', default='participant_id',
                    help='The header of subject column')
parser.add_argument('-ses', '--session-header', default='session_id',
                    help='The header of session column')
parser.add_argument('-icv', '--icv', default=False, action='store_true',
                    help='show icv')
args = parser.parse_args()

import numpy as np
import pandas as pd


def print_numbers_of_sessions(df):
    grouped= df.groupby(args.subject_header)
    num_sessions = grouped.apply(lambda x: len(x[args.session_header].unique()))
    for ns in sorted(num_sessions.unique()):
        print('Num of subjects with', ns, 'sessions:', np.sum(num_sessions==ns))

def split_sexes(df):
    sex_grouped = df.groupby('sex')
    males = list()
    females = list()
    if 'M' in sex_grouped.groups:
        males.append(sex_grouped.get_group('M'))
    if 'F' in sex_grouped.groups:
        females.append(sex_grouped.get_group('F'))
    if 1 in sex_grouped.groups:
        males.append(sex_grouped.get_group(1))
    if 0 in sex_grouped.groups:
        females.append(sex_grouped.get_group(0))
    males = pd.concat(males)
    females = pd.concat(females)
    return males, females

def print_ages(df):
    partial_df = df[[args.subject_header, 'age']]
    partial_grouped = partial_df.groupby(by=args.subject_header, group_keys=False)
    sorted = partial_grouped.apply(lambda x: x.sort_values(by='age'))
    sorted = sorted.groupby(by=args.subject_header)
    baseline_ages = sorted.first()
    mean_ba = baseline_ages.mean()
    std_ba = baseline_ages.std()
    lower_ba = baseline_ages.min()
    upper_ba = baseline_ages.max()
    print()
    message = 'Baseline ages %.4f ± %.4f, [%.4f, %.4f]'
    message = message % (mean_ba, std_ba, lower_ba, upper_ba)
    print(message)
    followup = sorted.apply(lambda x: x['age'].iloc[1:] - x['age'].iloc[0])
    mean_followup  = followup.mean()
    std_followup = followup.std()
    lower_fu = followup.min()
    upper_fu = followup.max()
    # print(df.loc[df.groupby(args.subject_header)['age'].diff().idxmax()])
    print('Followup time (following minus the first) %.4f ± %.4f, [%.2f, %.2f]'\
          % (mean_followup, std_followup, lower_fu, upper_fu))

def print_icv(df):
    grouped = df.groupby(args.subject_header)['icv'].first()
    grouped = grouped
    mean_icv = grouped.mean()
    std_icv = grouped.std()
    min_icv = grouped.min()
    max_icv = grouped.max()
    print('ICV %.2f ± %.2f [%.2f--%.2f]' % (mean_icv, std_icv, min_icv, max_icv))

df = pd.read_csv(args.input, sep=args.delimiter)
subject_grouped = df.groupby(args.subject_header)

# Print sessions
print('Num unique subjects:', len(subject_grouped))
print('Num unique sessions:', len(df.groupby([args.subject_header, args.session_header])))
print()
print_numbers_of_sessions(df)

# Print ages
print_ages(df)
if args.icv:
    print_icv(df)

# Print sexes
males, females = split_sexes(df)
num_males = len(males.groupby(args.subject_header))
num_females = len(females.groupby(args.subject_header))
if args.separate_sex:
    print()
    print('Male (%d):' % num_males)
    print('Num unique sessions:', len(males.groupby([args.subject_header, args.session_header])))
    print_numbers_of_sessions(males)
    print_ages(males)
    if args.icv:
        print_icv(males)
    print()
    print('Female (%d):' % num_females)
    print('Num unique sessions:', len(females.groupby([args.subject_header, args.session_header])))
    print_numbers_of_sessions(females)
    print_ages(females)
    if args.icv:
        print_icv(females)
else:
    print()
    print('Num males: %d; num females: %d' % (num_males, num_females))

# Print diagnosis
if 'diagnosis' in df: 
    diagnosis_grouped = df.groupby('diagnosis')
    if not args.separate_diagnosis:
        print()
    for diagnosis, values  in diagnosis_grouped:
        num_subjects = len(values.groupby(args.subject_header))
        males, females = split_sexes(values)
        num_males = len(males.groupby(args.subject_header))
        num_females = len(females.groupby(args.subject_header))
        baseline_ages = values.groupby(args.subject_header).apply(lambda x: x['age'].min())
        mean_ba = baseline_ages.mean()
        std_ba = baseline_ages.std()
        lower_ba = baseline_ages.min()
        upper_ba = baseline_ages.max()
        intervals = values[[args.subject_header, 'age']].groupby(args.subject_header).diff().dropna()
        mean_intervals = intervals.mean()
        std_intervals = intervals.std()
        if args.separate_diagnosis:
            print()
            print('Diagosis %s (#sub, M/F: %d, %d/%d)' % (str(diagnosis),
                                                          num_subjects,
                                                          num_males,
                                                          num_females))
            print_numbers_of_sessions(values)
        else:
            print('Diagosis %s: #sub %d, M/F %d/%d' % (str(diagnosis),
                                                       num_subjects,
                                                       num_males,
                                                       num_females))
        message = 'Baseline ages %.4f ± %.4f, [%.4f, %.4f]'
        message = message % (mean_ba, std_ba, lower_ba, upper_ba)
        print(message)
        print('Followup time intervals %.4f ± %.4f' % (mean_intervals, std_intervals))
    if args.print_multiple_diagnosis:
        func = lambda x: len(x['diagnosis'].unique())
        num_diagosis = subject_grouped.apply(func)
        indices = (num_diagosis > 1).values
        multiple_diagnosis = np.array(list(subject_grouped.groups))[indices]
        print()
        print('Num subject with multiple diagnosis:', len(multiple_diagnosis))
        selected = df[df[args.subject_header].isin(multiple_diagnosis)]
        for subject, values in selected.groupby(args.subject_header):
            print()
            print(subject)
            print(values[[args.session_header, 'age', 'diagnosis']])
