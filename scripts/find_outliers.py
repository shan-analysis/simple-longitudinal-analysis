#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input', required=True, help='Input .csv dataframe')
parser.add_argument('-y', '--y-variable', required=True,
                    help='The y variable in the lme fitting for the input df')
parser.add_argument('-o', '--output', help='Output file', required=True)
parser.add_argument('-p', '--prediction', help='LME prediction', required=True)
parser.add_argument('-v', '--varcor', help='LME VarCor', required=True)
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-gs', '--intercept-std-threshold', type=float, default=3,
                    help='The threshold for detecting subject-wise outlier')
parser.add_argument('-rs', '--residual-std-threshold', type=float, default=3,
                    help='The threshold for detecting visit-wise outlier')
parser.add_argument('-s', '--subject-header', default='participant_id',
                    help='The header for the subject column')
args = parser.parse_args()

import os
import pandas as pd


input_df = pd.read_csv(args.input, sep=args.delimiter)
input_df = input_df.set_index(args.subject_header)
input_df = input_df[args.y_variable]

pred_df = pd.read_csv(args.prediction, sep=args.delimiter, index_col=0)
pred_df = pred_df.set_index(args.subject_header)
pred_fixed_header = 'predict.fixed'
pred_subj_header = 'predict.%s' % args.subject_header

var_df = pd.read_csv(args.varcor, sep=args.delimiter, index_col=0)
intercept_std = var_df.loc['(Intercept)', 'StdDev']
residual_std = var_df.loc['Residual', 'StdDev']

# ASSUME THE FIRST DATA POINT FOR A SUBJECT CORRESPOINDS TO followup_time = 0
group_diff = (pred_df[pred_fixed_header] - input_df)
group_diff = group_diff.groupby(group_diff.index).first().abs()
ind = (group_diff / intercept_std) > args.intercept_std_threshold
group_outliers = list(group_diff[ind].index)
group_outliers = ','.join([str(num) for num in group_outliers])
print(group_outliers)

visit_diff = (pred_df[pred_subj_header] - input_df).abs()
visit_diff = visit_diff.groupby(visit_diff.index).max()
ind = (visit_diff / residual_std) > args.residual_std_threshold
visit_outliers = list(visit_diff[ind].index)
visit_outliers = ','.join([str(num) for num in visit_outliers])
print(visit_outliers)

with open(args.output, 'w') as csvfile:
    csvfile.write('\n'.join([group_outliers, visit_outliers]))
