#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, help='Input in long format')
parser.add_argument('-o', '--output', required=True, help='Output file')
parser.add_argument('-c', '--column-name', required=True, nargs='+',
                    help='The column to perform icc')
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-s', '--subject-header', default='participant_id',
                    help='The header for the subject column')
parser.add_argument('-n', '--decimal', default=2, type=int,
                    help='The number of decimals of ICC')
args = parser.parse_args()

import pandas as pd
import rpy2.robjects as ro
from collections import OrderedDict
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
pandas2ri.activate()


base = importr('base')
nlme = importr('nlme')
df = pd.read_csv(args.input, sep=args.delimiter)

icc = list()
control = nlme.lmeControl(opt='optim')
for column in args.column_name:
    print(column)
    col_new = column.replace('-', '_')
    df = df.rename(columns={column: col_new})
    tmp = OrderedDict()
    tmp['Region'] = column.replace('-', '--').replace('_', ' ')
    fixed = ro.Formula('%s ~ 1 + age' % col_new)
    random = ro.Formula('~ 1 | %s' % args.subject_header)
    result = nlme.lme(fixed=fixed, random=random, data=df, control=control)
    between_group_var = float(nlme.VarCorr(result)[0])
    within_group_var = float(result.rx2('sigma')[0]) ** 2
    tmp['ICC'] = between_group_var / (between_group_var + within_group_var)
    icc.append(tmp)

icc = pd.DataFrame(icc).round(args.decimal)
icc.to_csv(args.output, index=False)
