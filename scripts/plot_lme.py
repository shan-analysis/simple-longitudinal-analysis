#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
parser = argparse.ArgumentParser(description='Plot lme result')
parser.add_argument('-i', '--input', nargs='+',
                    help='Input table of LME result')
parser.add_argument('-o', '--output', default=None, help='Output figure file')
parser.add_argument('-b', '--baseline-age', nargs='+', type=float,
                    default=list(range(55, 90, 5)),
                    help='The absolute baseline_age used in the plot')
parser.add_argument('-t', '--time-intervals', nargs='+', type=float,
                    default=list(range(5)),
                    help='The time intervals between age and the baseline_age')
parser.add_argument('-a', '--age', nargs='+', default=None, type=float,
                    help='Plot model with age instead of baseline_age')
help = 'Original data used to show spaghetti; if None, do not show spaghetti'
parser.add_argument('-d', '--original-data', nargs='+', help=help)
help = ('Number of random samples to plot in spaghetti; -1 is all data; '
        'if -d is not specified, -r has no effect')
parser.add_argument('-r', '--random-sample', default=-1, type=int, help=help)
parser.add_argument('-y', '--y-variable', nargs='+', default=[''],
                    help='The column in -d to plot, or the y axis label')
parser.add_argument('-yu', '--y-unit', default='',
                    help='The unit of the y axis')
parser.add_argument('-yl', '--ylim', default=None, type=float,
                    nargs=2, help='Y axis lim')
parser.add_argument('-ys', '--ylim-same', default=0, type=int,
                    choices={0, 1, 2},
                    help='0: not same; 1: same per row; 2: all same')
parser.add_argument('-l', '--overlap-cols', default=False, action='store_true',
                    help='Overlap columns')
parser.add_argument('-m', '--age-mean', default=70, type=float,
                    help='The mean of age centered around in lme fitting')
help = ('The ICV to use in the plot. It should be 0 when ICV is centered '
        'around the mean during fitting')
parser.add_argument('-c', '--icv-mean', default=0, type=float, help=help)
parser.add_argument('-s', '--subject-header', default='participant_id',
                    help='The header for the subject column')
parser.add_argument('-n', '--drop-insignificant-interaction',
                    default=False, action='store_true',
                    help='Drop insignificant interaction columns')
help = ('Drop insignificant interactions with p greater than this value. '
        'It is also used to determine significancy if text shown')
parser.add_argument('-p', '--pval', default=0.05, type=float, help=help)
parser.add_argument('-st', '--show-text', action='store_true', default=False,
                    help='Show significancy text')
parser.add_argument('-tl', '--tile', default=[1, 1], nargs=2, type=int,
                    help='The tiles of the subplots')
parser.add_argument('-x', '--balance-sex', default=False, action='store_true',
                    help='Balance sex, use 0.5 for male, -0.5 for female')
args = parser.parse_args()

import os
import numpy as np
import pandas as pd
import matplotlib

matplotlib.use('Agg')
font = {'size': 7}
matplotlib.rc('font', **font)

import matplotlib.pyplot as plt

from simple_longitudinal_analysis import Spaghetti, LME
from simple_longitudinal_analysis import describe, select_random_subjects


assert len(args.input) == len(args.y_variable)
if args.original_data is not None:
    assert len(args.original_data) == len(args.y_variable)
assert np.prod(args.tile) >= len(args.input)

title_top = 0.1
rspace = 0.06
wspace = 0.5
lspace = wspace + 0.2
bspace=0.4
tspace = 0.01
sub_width = 3.15
sub_height = 2
width = sub_width * args.tile[1] + wspace * (args.tile[1] - 1) + lspace + rspace
height = sub_height * args.tile[0] + bspace + tspace
fig = plt.figure(figsize=(width, height))


def calc_ylim(volumes):
    ylim = np.quantile(volumes, (0.05, 0.95))
    return ylim

def calc_yticks(ylim):
    steps = np.array([10000, 5000, 1000, 500, 100, 50])
    num_ticks = 6
    step = (ylim[1] - ylim[0]) / num_ticks
    step_idx = np.argmin(np.abs(steps - step))
    step = steps[step_idx]
    ylim[0] = (np.round(ylim[0] / step) - 1) * step
    ylim[1] = (np.round(ylim[1] / step) + 1) * step
    yticks = np.arange(ylim[0], ylim[1], step)[1:]
    return ylim, yticks


if args.age is not None:
    min_age = np.min(args.age)
    max_age = np.max(args.age) + 1
    args.age = np.array(args.age) - args.age_mean
else:
    min_age = np.min(args.baseline_age) + np.min(args.time_intervals)
    max_age = np.max(args.baseline_age) + np.max(args.time_intervals) + 1
    args.baseline_age = np.array(args.baseline_age) - args.age_mean


ylim_all = list()
yticks_all = list()
axes = list()

for i, (input, y_variable) in enumerate(zip(args.input, args.y_variable)):
    coef_orig = pd.read_csv(input, index_col=0)

    coef_to_drop = list()
    if args.drop_insignificant_interaction:
        for ind, row in coef_orig.iterrows():
            if ':' in ind:
                if row['p-value'] > args.pval:

                    coef_to_drop.append(ind)
    coef = coef_orig.drop(index=coef_to_drop)
    print(coef)

    if args.age is not None:
        lme = LME.from_age(coef, ages=args.age, balance_sex=args.balance_sex)
    else:
        lme = LME.from_baseline_age(coef, baseline_ages=args.baseline_age,
                                    followup_times=args.time_intervals,
                                    icv=args.icv_mean,
                                    balance_sex=args.balance_sex)

    title = y_variable.replace('_', ' ')

    ax = plt.subplot(args.tile[0], args.tile[1], i+1)
    axes.append(ax)

    if i == 0:
        ylabel = 'Volume'
        if args.y_unit:
            ylabel = '%s (%s)' % (ylabel, args.y_unit)
    else:
        ylabel = ''
    legend = True if i == 0 else False
    ax = lme.plot(ax=ax, age_mean=args.age_mean, ylabel=ylabel, linewidth=1,
                  legend=legend)
    yticks = None

    if args.show_text:
        import textwrap
        check_mark = '\u2713'
        cross_mark = '\u2717'
        significancy = list()
        for ind, row in coef_orig.iterrows():
            if row['p-value'] < args.pval:
                significancy.append('%s: %s' % (ind, check_mark))
            else:
                significancy.append('%s: %s' % (ind, cross_mark))
        title = ', '.join(significancy)
        title = '\n'.join(textwrap.wrap(title, 80))
        plt.title(title)

    if args.ylim is not None:
        ylim = args.ylim

    if args.original_data is not None:
        orig_data = args.original_data[i]
        df = pd.read_csv(orig_data)
        df = df.rename(columns={args.subject_header: 'participant_id'})
        if args.random_sample > 0:
            df = select_random_subjects(df, args.random_sample)
        spaghetti = Spaghetti(df, separate_sex='all', linestyle='-.', alpha=0.2)
        spaghetti.plot('age', y_variable, ax=ax, legend=legend)
        volumes = df.groupby('participant_id')[y_variable].min()
        ylim = calc_ylim(volumes)
        ylim, yticks = calc_yticks(ylim)
    else:
        ylim = ax.get_ylim()

    ylim_all.append(ylim)
    yticks_all.append(yticks)

    ax.text(0.5, 1 - title_top/sub_height, title, transform=ax.transAxes,
            va='top', ha='center')
    #, bbox=dict(facecolor='white', edgecolor='gray', boxstyle='round'))

    if i < len(args.input) - args.tile[1]:
        plt.tick_params(axis='x', which='both', bottom=False, top=False,
                        labelbottom=False)

if args.ylim_same == 0:
    for ax, ylim, yticks in zip(axes, ylim_all, yticks_all):
        ax.set_ylim(ylim)
        if yticks is not None:
            ax.set_yticks(yticks)
        ax.set_xlim((min_age, max_age))

elif args.ylim_same == 1:
    if args.overlap_cols:
        wspace = 0
    ylim_all = np.array(ylim_all)
    for row_ind in range(args.tile[0]):
        tmp = ylim_all[row_ind*args.tile[1]:row_ind*args.tile[1] + args.tile[1], :]
        if tmp.size == 0:
            continue
        print('----------------')
        print(row_ind)
        print(tmp)
        print(ylim_all)

        ylim = [None] * 2
        ylim[0] = np.min(tmp[:, 0])
        ylim[1] = np.max(tmp[:, 1])
        ylim, yticks = calc_yticks(ylim)

        for col_ind in range(args.tile[1]):
            ind = row_ind * args.tile[1] + col_ind
            if ind < len(axes):
                axes[ind].set_ylim(ylim)
                if yticks is not None:
                    axes[ind].set_yticks(yticks)
                axes[ind].set_xlim((min_age, max_age))
            if args.overlap_cols:
                if col_ind > 0:
                    axes[ind].tick_params(axis='y', which='both',
                                          left=False, right=False,
                                          labelleft=False, labelright=False)
            # else:
            #     plt.grid('on')


plt.subplots_adjust(left=lspace/width, right=1-rspace/width,
                    wspace=wspace/sub_width, top=1-tspace/height,
                    bottom=bspace/height, hspace=0)
fig.savefig(args.output)
