#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, help='Input file')
parser.add_argument('-o', '--output', help='Output file', default=None)
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-S', '--separate-sex', default='all',
                    choices={'male', 'female', 'none', 'all'},
                    help='Seperate male and female in the plot')
parser.add_argument('-r', '--random-sample', default=-1, type=int,
                    help='Number of random samples to plot; -1 is all data')
parser.add_argument('-a', '--age-range', nargs=2, default=[-1, -1], type=float,
                    help='Range of age to plot; -1 is all ages')
parser.add_argument('-y', '--y-variable', help='The y variable')
parser.add_argument('-x', '--x-variable', default='age', help='The x variable')
parser.add_argument('-b', '--show-subject', action='store_true', default=False,
                    help='Show subjects on the plots')
parser.add_argument('-H', '--highlight', nargs='+', default=list(),
                    help='Highlight subjects')
parser.add_argument('-ho', '--highlight-only', action='store_true',
                    default=False, help='Only show highlighted subjects')
parser.add_argument('-s', '--subject-header', default='participant_id',
                    help='The header for the subject column')
parser.add_argument('-p', '--prediction', default=None,
                    help='The predicted data from the fit model')
args = parser.parse_args()

import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from simple_longitudinal_analysis import Spaghetti, describe
from simple_longitudinal_analysis import select_age, select_random_subjects


lower_age = float('-inf') if args.age_range[0] < 0 else args.age_range[0]
upper_age = float('inf') if args.age_range[1] < 0 else args.age_range[1]

df = pd.read_csv(args.input, sep=args.delimiter)
df = df.rename(columns={args.subject_header: 'participant_id'})
df = select_age(df, lower_age, upper_age)
if args.random_sample > 0:
    df = select_random_subjects(df, args.random_sample)
describe(df)

if args.prediction is not None:
    pred = pd.read_csv(args.prediction, index_col=0)
    pred = pred.rename(columns={'predict.'+args.subject_header:
                                'predict.participant_id',
                                args.subject_header: 'participant_id'})
else:
    pred = None

spaghetti = Spaghetti(df, separate_sex=args.separate_sex,
                      show_subject=args.show_subject)

print(args.y_variable)
fig = plt.figure()
spaghetti.plot(args.x_variable, args.y_variable, args.highlight,
               highlight_only=args.highlight_only, prediction=pred)
plt.tight_layout()

if args.output is None:
    plt.show()
else:
    fig.savefig(args.output)
