#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, help='Input table')
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-o', '--output-dir', nargs='+',
                    help='Output dir; must have the same length with -y')
parser.add_argument('-y', '--y-variable', nargs='+',
                    help='The y variable; must have the same length with -o')
fixed_formula = ('1 + icv + sex + baseline_age + followup_time '
                 '+ baseline_age : followup_time + sex : followup_time')
parser.add_argument('-f', '--fixed-formula', help='Fixed effect RHS formula',
                    default=fixed_formula)
parser.add_argument('-r', '--random-formula', help='Random effect RHS formula',
                    default='1 + followup_time | participant_id')
parser.add_argument('-a', '--age-mean', default=65, type=float,
                    help='Center the age around this number')
parser.add_argument('-c', '--icv-mean', default=1400000, type=float,
                    help='Center the age around this number')
parser.add_argument('-v', '--verbose', action='store_true', default=False,
                    help='Print covariance and sigma')
parser.add_argument('-s', '--subject-header', default='participant_id',
                    help='The header for the subject column')
parser.add_argument('-l', '--outliers', nargs='+', default=list(),
                    help='Subject name of outliers')
parser.add_argument('-x', '--balance-sex', action='store_true',
                    help='Use 0.5 as male and -0.5 as female')
args = parser.parse_args()

import numpy as np
import os
import pandas as pd
import rpy2.robjects as ro
from collections import OrderedDict
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
pandas2ri.activate()


if len(args.y_variable) != len(args.output_dir):
    raise RuntimeError('-y and -o should have the same length')

if not args.fixed_formula.lstrip().startswith('~'):
    args.fixed_formula = '~' + args.fixed_formula
if not args.random_formula.lstrip().startswith('~'):
    args.random_formula = '~' + args.random_formula

base = importr('base')
nlme = importr('nlme')
stats = importr('stats')
utils = importr('utils')

df = pd.read_csv(args.input, sep=args.delimiter)
outliers = df[args.subject_header].isin(args.outliers)
df = df[~outliers]

df['age'] = df['age'] - args.age_mean
df['icv'] = df['icv'] - args.icv_mean

if args.balance_sex:
    df.loc[df['sex']==1, 'sex'] = 0.5
    df.loc[df['sex']==0, 'sex'] = -0.5

insert_baseline_age = (('baseline_age' in args.fixed_formula) \
    or ('followup_time' in args.fixed_formula))
if insert_baseline_age:
    df['baseline_age'] = df.groupby(args.subject_header)['age'].transform('min')
    df['followup_time'] = df['age'] - df['baseline_age']
df.to_csv('test.csv', index=False)

print(df.head())

control = nlme.lmeControl(opt='optim')
for y, output_dir in zip(args.y_variable, args.output_dir):
    y_new = y.replace('-', '_')
    df = df.rename(columns={y: y_new})

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    fixed = ro.Formula(y_new + args.fixed_formula)
    random = ro.Formula(args.random_formula)
    print('-' * 80)
    print(y, output_dir)
    print()
    print('Fixed effects:', fixed)
    print('Random effects:', random)
    print(df.head())
    fit = nlme.lme(fixed=fixed, random=random, data=df, control=control,
                   na_action=stats.na_omit)
    summary = base.summary(fit)
    coef = summary.rx2('tTable')
    varcor = nlme.VarCorr(fit)
    pred = nlme.predict_lme(fit, df, level=np.array([0, 1]),
                            na_action=stats.na_omit)
    if args.verbose:
        print(coef)

    coef_filename = os.path.join(output_dir, 'coef.csv')
    utils.write_csv(coef, coef_filename, quote=False)
    varcor_filename = os.path.join(output_dir, 'varcor.csv')
    utils.write_csv(varcor, varcor_filename, quote=False)
    pred_filename = os.path.join(output_dir, 'pred.csv')
    utils.write_csv(pred, pred_filename, quote=False)
