#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--target', required=True,
                    help='The dataframe to match to')
parser.add_argument('-s', '--source', required=True,
                    help='The dataframe to match from')
parser.add_argument('-o', '--output', required=True, help='Matched dataframe')
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-m', '--match-mode', choices={'age', 'sex', 'age_sex'},
                    help='The matching mode')
args = parser.parse_args()

import pandas as pd
from simple_longitudinal_analysis import describe, MatcherFactory


print('-' * 80)
target_df = pd.read_csv(args.target, sep=args.delimiter)
print('Target:')
describe(target_df)
print('-' * 80)
source_df = pd.read_csv(args.source, sep=args.delimiter)
print('Source:')
describe(source_df)
print('-' * 80)

matcher = MatcherFactory.get_matcher(target_df, mode=args.match_mode)
matched_df = matcher.match_from(source_df)

print('Matched dataframe:')
describe(matched_df)
print('-' * 80)
print('Matching statistical test(s):')
for key, value in matcher.validate(matched_df).items():
    print(key, value)

matched_df.to_csv(args.output, index=False)
