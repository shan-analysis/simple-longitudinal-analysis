#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', required=True, help='Input table')
parser.add_argument('-d', '--delimiter', help='Delimiter', default=',')
parser.add_argument('-o', '--output', nargs='+',
                    help='Output file; must have the same length with -y')
parser.add_argument('-y', '--y-variable', nargs='+',
                    help='The y variable; must have the same length with -o')
parser.add_argument('-f', '--formula', help='Right-hand-side formula',
                    default='1 + icv + sex + age + sex : age')
args = parser.parse_args()

import pandas as pd
import rpy2.robjects as ro
from collections import OrderedDict
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
pandas2ri.activate()


if len(args.y_variable) != len(args.output):
    raise RuntimeError('-y and -o should have the same length')

if not args.formula.lstrip().startswith('~'):
    args.formula = '~' + args.formula

base = importr('base')
stats = importr('stats')
utils = importr('utils')
df = pd.read_csv(args.input, sep=args.delimiter)

if (('baseline_age' in args.formula) or ('followup_time' in args.formula))\
        and (('baseline_age' not in df) or ('followup_time' not in df)):
    df['baseline_age'] = df.groupby('participant_id')['age'].transform('min')
    df['followup_time'] = df['age'] - df['baseline_age']

for y, o in zip(args.y_variable, args.output):
    formula = ro.Formula(y + args.formula)
    print('Formula:', formula)
    fit = stats.lm(formula=formula, data=df, na_action=stats.na_omit)
    result = base.summary(fit).rx2('coefficients')
    utils.write_csv(result, o, quote=False)
